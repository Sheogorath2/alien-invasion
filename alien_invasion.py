import pygame
from pygame.sprite import Group

import game_functions as gf
from settings import Settings
from ship import Ship
from game_stats import GameStats


def run_game():
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")

    enable_sound = False

    sound_bullet = None

    if enable_sound:
        pygame.mixer.music.load(ai_settings.music_danger)
        pygame.mixer.music.play(10)
        sound_bullet = pygame.mixer.Sound(ai_settings.sound_bullet)

    ship = Ship(ai_settings, screen)
    bullets = Group()
    aliens = Group()
    gf.create_fleet(ai_settings, ship, aliens)
    stats = GameStats(ai_settings)
    while True:
        gf.check_events(ai_settings, screen, ship, bullets, sound_bullet)
        if (stats.game_active):
            ship.update()
            gf.update_aliens(ai_settings, stats, ship, aliens, bullets)
            bullets.update()
            for bullet in bullets.copy():
                if bullet.y < 0:
                    bullets.remove(bullet)

        gf.update_screen(ai_settings, screen, ship, aliens, bullets)


run_game()
