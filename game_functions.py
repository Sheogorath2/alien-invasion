import sys

import pygame

from alien import Alien
from bullet import Bullet
from time import sleep


def check_events(ai_settings, screen, ship, bullets, bullet_sound):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            check_keydown_events(event, ai_settings, screen, ship, bullets, bullet_sound)
        elif event.type == pygame.KEYUP:
            check_keyup_events(event, ship)


def check_keydown_events(event, ai_settings, screen, ship, bullets, bullet_sound):
    if event.key == pygame.K_q:
        sys.exit()
    if event.key == pygame.K_RIGHT:
        ship.moving_right = True
    if event.key == pygame.K_LEFT:
        ship.moving_left = True
    if event.key == pygame.K_SPACE:
        fire_bullet(ai_settings, screen, ship, bullets, bullet_sound)


def check_keyup_events(event, ship):
    if event.key == pygame.K_RIGHT:
        ship.moving_right = False
    if event.key == pygame.K_LEFT:
        ship.moving_left = False


def update_screen(ai_settings, screen, ship, aliens, bullets):
    screen.fill(ai_settings.bg_color)
    update_bullets(ai_settings, ship, aliens, bullets)
    ship.blitme()
    aliens.draw(screen)
    pygame.display.flip()

def update_bullets(ai_settings, ship, aliens, bullets):
    check_bullet_alien_collisions(ai_settings, ship, aliens, bullets)
    for bullet in bullets.sprites():
        bullet.draw()

def check_bullet_alien_collisions(ai_settings, ship, aliens, bullets):
    pygame.sprite.groupcollide(bullets, aliens, True, True)
    if len(aliens) == 0:
        bullets.empty()
        create_fleet(ai_settings, ship, aliens)

def fire_bullet(ai_settings, screen, ship, bullets, bullet_sound):
    if len(bullets) < ai_settings.bullets_allowed:
        new_bulet = Bullet(ai_settings, screen, ship)
        bullets.add(new_bulet)
        if bullet_sound:
            bullet_sound.play()


def get_number_aliens_x(ai_settings, alien_width):
    avaliable_space_x = ai_settings.screen_width - 2 * alien_width
    number_alines_x = avaliable_space_x // (2 * alien_width)
    return number_alines_x


def get_number_rows(ai_settings, ship_height, alien_height):
    avaliable_space_y = ai_settings.screen_height - 3 * alien_height - ship_height
    number_rows = avaliable_space_y // (2 * alien_height)
    return number_rows


def create_alien(ai_settings, aliens, alien_number, row_number):
    alien = Alien(ai_settings)
    alien_width = alien.rect.width
    alien_height = alien.rect.height
    alien_x = alien_width + 2 * alien_width * alien_number
    alien_y = alien_height + 2 * alien_height * row_number
    alien.x = alien_x
    alien.y = alien_y
    aliens.add(alien)

def ship_hit(ai_settings, stats, ship, aliens, bullets):
    stats.ships_left -= 1
    if (stats.ships_left > 0):

        aliens.empty()
        bullets.empty()

        create_fleet(ai_settings, ship, aliens)
        ship.center_ship()

        sleep(0.5)
    else:
        stats.game_active = False

def update_aliens(ai_settings, stats, ship, aliens, bullets):
    check_fleet_edges(ai_settings, aliens)
    check_aliens_bottom(ai_settings, stats, ship, aliens, bullets)
    if (pygame.sprite.spritecollideany(ship, aliens)):
        ship_hit(ai_settings, stats, ship, aliens, bullets)
    aliens.update()

def create_fleet(ai_settings, ship, aliens):
    alien = Alien(ai_settings)
    number_aliens_x = get_number_aliens_x(ai_settings, alien.rect.width)
    number_rows = get_number_rows(ai_settings, ship.rect.height, alien.rect.height)
    for row_number in range(number_rows):
        for alien_number in range(number_aliens_x):
            create_alien(ai_settings, aliens, alien_number, row_number)

def check_fleet_edges(ai_settings, aliens):
    for alien in aliens.sprites():
        if alien.check_edges():
            change_fleet_direction(ai_settings, aliens)
            break

def change_fleet_direction(ai_settings, aliens):
    for alien in aliens.sprites():
        alien.y += ai_settings.fleet_drop_speed
    ai_settings.fleet_direction *= -1

def check_aliens_bottom(ai_settings, stats, ship, aliens, bullets):
    for alien in aliens.sprites():
        if alien.rect.bottom >= ai_settings.screen_height:
            ship_hit(ai_settings, stats, ship, aliens, bullets)
            break

