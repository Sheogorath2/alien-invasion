import pygame
from pygame.sprite import Sprite


class Alien(Sprite):
    def __init__(self, ai_settings):
        super(Alien, self).__init__()

        self.ai_settings = ai_settings

        self.image = pygame.image.load(ai_settings.alien_image_filename)
        self.image = pygame.transform.scale(self.image, (self.ai_settings.alien_width, self.ai_settings.alien_height))

        self.rect = self.image.get_rect()

        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        self.x = float(self.rect.x)
        self.y = self.rect.y

    def check_edges(self):
        if self.rect.right >= self.ai_settings.screen_width or self.rect.left <= 0:
            return True

    def blitme(self):
        self.screen.blit(self.image, self.rect)

    def update(self):
        self.x += self.ai_settings.alien_speed_factor * self.ai_settings.fleet_direction
        self.rect.x = self.x
        self.rect.y = self.y

