class Settings:
    def __init__(self):
        self.screen_width = 1280
        self.screen_height = 800
        self.bg_color = (230, 230, 230)

        self.ship_image_filename = "images/ship.png"
        self.alien_image_filename = "images/alien.png"

        self.ship_width = 55
        self.ship_height = 160

        self.alien_width = 50
        self.alien_height = 50

        self.ship_speed_factor = 1.5

        self.bullet_speed_factor = 1

        self.alien_speed_factor = 3
        self.fleet_drop_speed = 10
        self.fleet_direction = 1

        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = 60, 60, 60

        self.bullets_allowed = 3

        self.ships_limit = 3

        # Soundtrack from Freelancer
        self.music_idle = "audio/music_omicron_space.wav"
        self.music_danger = "audio/music_omicron_danger.wav"
        self.music_battle = "audio/music_omicron_battle.wav"
        self.sound_bullet = "audio/fire_laser1.wav"
