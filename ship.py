import pygame


class Ship:
    def __init__(self, ai_settings, screen):
        self.ai_settings = ai_settings
        self.screen = screen

        self.image = pygame.image.load(ai_settings.ship_image_filename)
        self.image = pygame.transform.scale(self.image, (self.ai_settings.ship_width, self.ai_settings.ship_height))
        self.rect = self.image.get_rect()
        self.screen_rect = self.screen.get_rect()

        self.center = float(self.screen_rect.centerx)
        self.rect.bottom = self.screen_rect.bottom

        self.moving_right = False
        self.moving_left = False

    def blitme(self):
        self.screen.blit(self.image, self.rect)

    def center_ship(self):
        self.x = self.screen_rect.centerx

    def update(self):
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.center += self.ai_settings.ship_speed_factor
        if self.moving_left and self.rect.left > self.screen_rect.left:
            self.center -= self.ai_settings.ship_speed_factor
        self.rect.centerx = self.center
